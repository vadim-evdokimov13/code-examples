import axios from 'axios'
import qs from 'qs'
import getApiRequests from './apiRequests.js'

const isServer = typeof window === 'undefined',
      isProd = process.env.NODE_ENV !== 'development'

/**
 * @class LogicError
 * @description used only to suppress bluebird error when non-error object rejected
 * */
class LogicError extends Error {
  constructor (message, code) {
    super()
    this.message = message
    this.code = code
    this.stack = null
  }
}

function buildNetworkErrorModalData (errDetails) {
  const msgParts = []

  if (errDetails.env === 'node') {
    msgParts.push('n')
  }
  else {
    msgParts.push('b')
  }

  if (errDetails.who === 'server') {
    msgParts.push('s')
  }
  else {
    msgParts.push('c')
  }

  if (errDetails.code) {
    msgParts.push(errDetails.code)
  }

  return {
    name: 'c-network-error',
    data: msgParts.join(':'),
    close: null
  }
}

function wrapBackend (backend, context, storeWrapper) {
  let startRecord,
      stopRecord

  if (isServer) {
    startRecord = require('../../server/timing.js').startRecord
    stopRecord = require('../../server/timing.js').stopRecord
  }

  function wrapper (method, reqName, url, ...args) {
    return new Promise((resolve, reject) => {
      const attachedStore = storeWrapper.attachedStore,
            storeAttached = Boolean(attachedStore)
      let taskIndex

      if (isServer) {
        startRecord(context, reqName)
      }
      else if (storeAttached) {
        taskIndex = attachedStore.dispatch('enqueueTask')
      }

      // No return here
      backend[method](url, ...args)
        .then(res => {
          if (isServer) {
            stopRecord(context, reqName)
          }
          else if (storeAttached) {
            attachedStore.dispatch('removeTask', taskIndex)
          }

          const data = res.data

          if (data.status === 'ok') {
            resolve(data.message)
          }
          else if (data.status === 'error') {
            reject(new LogicError(data.message, data.code))
          }
          else {
            reject(new LogicError(`Server-side API error in ${reqName} endpoint!\n${method.toUpperCase()} ${url} -> malformed response.`, -1))
          }
        })
        .catch(err => {
          const alreadyEmitted = attachedStore.state.modal.name === 'c-network-error',
                errDetails = {
                  env: isServer ? 'node' : 'browser',
                  who: 'server',
                  code: 0
                }

          if (err.response) {
            const resStatus = err.response.status

            errDetails.code = resStatus
            console.error(`Server-side API error in ${reqName} endpoint!\n${method.toUpperCase()} ${url} -> ${resStatus}
            `)
          }
          else if (err.request) {
            console.error(`Server-side API error in ${reqName} endpoint!\n${method.toUpperCase()} ${url} -> no response.
              `)
          }
          else {
            errDetails.who = 'client'
            console.error(`Client-side API error in ${reqName} endpoint!\nRequest was not sended.\n${err}
            `)
          }

          if (!alreadyEmitted) {
            attachedStore.commit('SET_MODAL', buildNetworkErrorModalData(errDetails))
          }

          if (isServer) {
            stopRecord(context, reqName)
            resolve(err)
          }
        })
    })
  }

  return {
    get: (url, ...rest) => reqName => wrapper('get', reqName, url, ...rest),
    post: (url, ...rest) => reqName => wrapper('post', reqName, url, ...rest)
  }
}

function assignReqNames (apiObject) {
  for (const reqName in apiObject) {
    if (apiObject.hasOwnProperty(reqName)) {
      const apiFunc = apiObject[reqName]

      apiObject[reqName] = (...args) => apiFunc(...args)(reqName)
    }
  }
}

export default function createAPI (context) {
  const axiosConfig = {
          baseURL: `${SERVER_PATH}/api/`,
          withCredentials: true,
          paramsSerializer: params => qs.stringify(params, {arrayFormat: 'brackets'}),
          validateStatus: st => st >= 200 && st < 299
        },
        storeWrapper = {attachedStore: null}

  // Set cookie header if it is Node environment
  if (isServer && context && context.cookie) {
    axiosConfig.headers = {Cookie: context.cookie}
  }

  // On production server we use unix socket
  // instead of http protocol
  if (isServer && isProd) {
    axiosConfig.socketPath = SOCKET_PATH
  }

  const originalBackend = axios.create(axiosConfig),
        backend = wrapBackend(originalBackend, context, storeWrapper),
        api = getApiRequests(backend, storeWrapper)

  assignReqNames(api)

  // Update attachedStore through closure link to it
  api.attachStore = store => {
    storeWrapper.attachedStore = store
  }

  return api
}
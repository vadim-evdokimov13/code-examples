import '../scss/index.scss'
import createApp from './app'
import * as Sentry from '@sentry/browser'

function runApp () {
  const {app, router, store} = createApp()

  if (window.__INITIAL_STATE__) {
    store.replaceState(window.__INITIAL_STATE__)
  }

  router.onReady(() => {app.$mount('#app')})
}

if (process.env.NODE_ENV === 'production') {
  Sentry.init({
    dsn: '...',
    environment: process.env.NODE_ENV
  })

  Sentry.configureScope(() => {
    runApp()
  })
}
else {
  runApp()
}
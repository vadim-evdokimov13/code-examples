import mutations from './mutations'

export default () => ({
  state: {
    name: '',
    data: null,
    close: null
  },
  mutations
})
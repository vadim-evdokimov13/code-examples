import './client/utils/polyfills'
import Vue from 'vue'
import createStore from './client/store'
import createRouter from './client/router'
import createBuses from './client/utils/bus.js'
import createAPI from './client/utils/api.js'
import VueMask from 'v-mask'
import VueImg from './client/components/vendor/v-img'
import VueBar from 'vuebar'
import VueScrollTo from 'vue-scrollto'
import prototypeMixins from './client/utils/prototypeMixins.js'
import VueViewer from 'v-viewer'


Vue.config.performance = process.env.NODE_ENV !== 'production'

// Define global fns and props
prototypeMixins(Vue)

export default function createApp (context) {
  Vue.use(VueMask)
  Vue.use(VueImg)
  Vue.use(VueBar)
  Vue.use(VueScrollTo)
  Vue.use(VueViewer)

  const {bus, promiseBus} = createBuses(),
        API = createAPI(context),
        store = createStore(context, API),
        router = createRouter(),
        app = new Vue({
          data: {bus, promiseBus},
          store,
          router,
          render: h => h('router-view')
        })

  return {app, router, store, bus, promiseBus}
}
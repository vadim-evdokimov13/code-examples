function cookiesToHeaders (storeWrapper) {
  const user = storeWrapper.attachedStore.state.user

  return {
    'X-GA-STNWLF': user.token,
    'X-GA-NRT': user.sex
  }
}

export default (backend, storeWrapper) => ({
  // User
  isSignedIn: () => backend.get('is_signed_in'),
  // Common
  getMetas: path => backend.get(`seo?path=${path}`),
  getHeadMenu: path => backend.get(`head_menu?path=${path}`),
  // Category
  getCat: (id, params) => backend.get(`categories/${id}`, {
    params,
    headers: cookiesToHeaders(storeWrapper)
  }),
  // Product
  deleteProd: data => backend.post('delete_product', {product_id: data.id}, {headers: cookiesToHeaders(storeWrapper)}),

})
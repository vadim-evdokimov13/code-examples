export default {
  'SET_MODAL' (state, modal) {
    Object.assign(state, modal)
  },
  'CLOSE_MODAL' (state) {
    state.name = ''
    state.data = null
    state.close = null
  }
}

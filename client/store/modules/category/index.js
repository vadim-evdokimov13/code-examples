import actions from './actions'
import mutations from './mutations'

export default API => ({
  state: {
    currentCategory: {},
    products: [],
    productsCount: 0,
    tree: [],
    merchant: {
      login: '',
      photo_url: ''
    },
    filtersData: {
      static: {
        pagesCount: 0,
        sort: [
          {
            name: 'По умолчанию',
            value: ''
          },
          {
            name: 'По возрастанию',
            value: 'priceAsc'
          },
          {
            name: 'По убыванию',
            value: 'priceDesc'
          }
        ],
        prices: [0, 9999999]
      },
      dyn: []
    },
    filtersVModel: {
      static: {
        page: 1,
        sort: ''
      },
      dyn: [] // 'price' is semi-static filter, but it's vmodel will be placed here
    }
  },
  actions: actions(API),
  mutations
})
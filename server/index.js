const server = require('express')(),
      {startRecord, stopRecord, setServerTimingHeader} = require('./timing.js'),
      path = require('path'),
      fs = require('fs'),
      cookie = require('cookie-parser'),
      spiderDetector = require('spider-detector'),
      templatePath = path.resolve(__dirname, './index.template.html'),
      createBundleRenderer = require('vue-server-renderer').createBundleRenderer,
      fileRegExp = /\.(?:\w)+\??/,
      uppercaseRegExp = /[A-ZА-Я]/,
      isProd = process.env.NODE_ENV !== 'development',
      // wrap renderer object to create object's link
      // due to assign operation
      rendererWrapper = {renderer: null}

function setHeaders (req, res, ctx) {

  // Disable nginx proxy buffering
  // http://nginx.org/docs/http/ngx_http_proxy_module.html#proxy_buffering

  res.setHeader('X-Accel-Buffering', 'no')

  stopRecord(ctx, 'Overall')
  setServerTimingHeader(ctx, res)

  for (const [key, value] of ctx.setCookie) {
    if (value) {
      res.cookie(key, value, {expires: new Date(Date.now() * 2)})
    }
    else {
      res.cookie(key, '', {expires: new Date(Date.now() / 2)})
    }
  }
}

function updateRenderer (bundle, options) {
  rendererWrapper.renderer = createBundleRenderer(bundle, options)
}

function render (req, res) {
  let context = {
    url: req.url,
    setCookie: new Map(),
    cookie: req.get('Cookie') || '',
    sex: req.cookies._ga_nrt || '',
    token: req.cookies._ga_stnwlf || '',
    status: {code: 200, url: ''},
    isBot: req.isSpider(),
    seo: {
      title: '',
      description: '',
      keywords: '',
      prevLink: '',
      nextLink: '',
      canonicalLink: ''
    },
    recordsMap: new Map()
  }

  startRecord(context, 'Overall')

  rendererWrapper.renderer.renderToString(context)
    .then(html => {
      const statusCode = context.status.code,
            redirectUrl = context.status.url,
            isBot = context.isBot

      setHeaders(req, res, context)

      if (statusCode === 200) {
        res.send(html)
      }
      else if (statusCode === 404) {
        if (isBot) {
          res.status(404).send(html)
        }
        else {
          res.redirect(301, '/notfound')
        }
      }
      else if (statusCode === 301) {
        res.redirect(301, redirectUrl)
      }

      // Clear memory after response was sended
      html = context = null // eslint-disable-line
    })
    .catch(err => {
      res.status(500).end()
      console.error(err)

      if (isProd) {
        process.exit(1)
      }
    })
}

server.set('case sensitive routing', true)
server.set('x-powered-by', false)
server.set('trust proxy', 'loopback')
server.use(cookie())
server.use(spiderDetector.middleware())

// Middleware checks for uppercase letters in url
// and redirect to lowercased url version
server.use((req, res, next) => {
  const reqPath = req.path

  if (uppercaseRegExp.test(reqPath)) {
    res.redirect(301, reqPath.toLowerCase())
  }
  else {
    next()
  }
})

if (isProd) {
  const template = fs.readFileSync(templatePath, 'utf-8') // eslint-disable-line
  const bundle = require('./vue-ssr-server-bundle.json')

  rendererWrapper.renderer = createBundleRenderer(bundle, {
    runInNewContext: false,
    template,
    cache: null
  })

  server.use(require('serve-static')(
    path.resolve(__dirname, '../client'),
    {index: false}
  ))

  server.use((req, res, next) => {
    if (fileRegExp.test(req.path)) {
      res.sendStatus(404)
      res.end()
    }
    else {
      next()
    }
  })

  server.get('*', render)

  server.listen(3000, 'localhost', () => {
    console.log('Server-side rendering process started at http://localhost:3000')
  })
    .on('connection', sock => sock.setNoDelay(true))
}
else {
  const setupDevServer = require('../../dev-server.js')

  setupDevServer(server, render, updateRenderer)
}
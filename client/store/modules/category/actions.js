const addPrefix = prefix => el => {
  el.originalUrl = el.url
  el.url = `/${prefix}/${el.url}`
}

// This method creates vmodel for dyn filters from data
// at runtime, because we get final filters list
// from backend and it is different for each category
function createDynFiltersVModel (dynFiltersData) {
  const newVModels = []

  dynFiltersData.forEach(f => {
    // Price is a special case =/
    // It's v-model always looks like [Number, Number]
    if (f.feature_name === 'price') {
      newVModels.push(f)
    }
    else {
      // For other filters we construct vmodel
      // only from checked values
      newVModels.push({
        feature_id: f.feature_id,
        feature_name: f.feature_name,
        values: f.values
          .filter(v => v.checked)
          .map(v => v.value)
      })
    }
  })

  return newVModels
}

const wellKnownFiltersNames = {
  price: 'Цена',
  brand_id: 'Бренд',
  size: 'Размер',
  condition: 'Состояние'
}

// Just add filterName key to each filter
function setFilterName (filter) {
  let filterName = filter.feature_name

  // Backend response-specific code
  if (filterName in wellKnownFiltersNames) {
    filterName = wellKnownFiltersNames[filterName]
  }

  return Object.assign({filterName}, filter)
}

export default API => ({
  getCat (store, {catId, params}) {
    return API.getCat(catId, params)
      .then(res => {
        res.products.forEach(addPrefix('p'))

        store.commit('SET_CURRENT_CATEGORY', res.category)
        store.commit('SET_CAT_TREE', res.tree)
        store.commit('SET_PRODUCTS', res.products)
        store.commit('SET_BREADCRUMBS', res.breadcrumbs)
        store.commit('SET_PRODUCTS_COUNT', res.products_count)
        store.commit('SET_DYN_FILTERS_DATA', res.filters.map(setFilterName))
        store.commit('SET_DYN_FILTERS_VMODEL', createDynFiltersVModel(res.filters))
        store.commit('SET_STATIC_FILTERS_DATA', {
          pagesCount: res.pages_count,
          prices: res.prices
        })
        store.commit('SET_STATIC_FILTERS_VMODEL', {
          page: res.page,
          sort: res.sort || ''
        })

        store.commit('SET_SEO_PAGINATION_CURRENT', res.page)
        store.commit('SET_SEO_PAGINATION_PAGES', res.pages_count)
      })
  },
  clearCurrentCategory (store) {
    store.commit('SET_CURRENT_CATEGORY', {})
    store.commit('SET_CAT_TREE', [])
    store.commit('SET_PRODUCTS', [])
    store.commit('SET_BREADCRUMBS', [])
    store.commit('SET_PRODUCTS_COUNT', 0)
    store.commit('SET_DYN_FILTERS_DATA', [])
    store.commit('SET_DYN_FILTERS_VMODEL', [])
    store.commit('SET_STATIC_FILTERS_DATA', {
      pagesCount: 0,
      prices: [0, 9999999]
    })
    store.commit('SET_STATIC_FILTERS_VMODEL', {
      page: 1,
      sort: ''
    })

    store.commit('SET_MERCHANT_DATA', {
      login: '',
      photo_url: ''
    })
  },
  resetFilters (store) {
    const catModule = store.state,
          filters = catModule.filters,
          filtersDefaults = catModule.filtersDefaults

    for (const key in filters) {
      if (filters.hasOwnProperty(key)) {
        store.commit('SET_FILTER', {key, value: filtersDefaults[key]})
      }
    }
  },
  getUserProducts (store, {catId, userId, params}) {
    return API.getUserProducts(catId, userId, params)
      .then(res => {
        res.products.forEach(addPrefix('p'))

        store.commit('SET_CURRENT_CATEGORY', res.category)
        store.commit('SET_CAT_TREE', res.tree)
        store.commit('SET_PRODUCTS', res.products)
        store.commit('SET_BREADCRUMBS', res.breadcrumbs)
        store.commit('SET_PRODUCTS_COUNT', res.products_count)
        store.commit('SET_DYN_FILTERS_DATA', res.filters.map(setFilterName))
        store.commit('SET_DYN_FILTERS_VMODEL', createDynFiltersVModel(res.filters))
        store.commit('SET_STATIC_FILTERS_DATA', {
          pagesCount: res.pages_count,
          prices: res.prices
        })
        store.commit('SET_STATIC_FILTERS_VMODEL', {
          page: res.page,
          sort: res.sort || ''
        })

        store.commit('SET_MERCHANT_DATA', res.user_info)
      })
  }
})

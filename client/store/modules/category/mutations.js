export default {
  'SET_CURRENT_CATEGORY' (state, category) {
    state.currentCategory = category
  },
  'SET_CAT_TREE' (state, tree) {
    state.tree = tree
  },
  'SET_PRODUCTS' (state, products) {
    state.products = products
  },
  'SET_PRODUCTS_COUNT' (state, productsCount) {
    state.productsCount = productsCount
  },
  'SET_MERCHANT_DATA' (state, data) {
    state.merchant = data
  },
  'SET_DYN_FILTERS_DATA' (state, dynFiltersData) {
    state.filtersData.dyn = dynFiltersData
  },
  'SET_DYN_FILTERS_VMODEL' (state, vmodel) {
    state.filtersVModel.dyn = vmodel
  },
  'UPDATE_DYN_FILTERS_VMODEL' (state, {index, values}) {
    state.filtersVModel.dyn[index].values = values
  },
  'UPDATE_STATIC_FILTERS_VMODEL' (state, {key, value}) {
    state.filtersVModel.static[key] = value
  },
  'SET_STATIC_FILTERS_DATA' (state, staticFiltersData) {
    const {pagesCount, prices} = staticFiltersData

    state.filtersData.static.pagesCount = pagesCount
    state.filtersData.static.prices = prices
  },
  'SET_STATIC_FILTERS_VMODEL' (state, vmodel) {
    const {page, sort} = vmodel

    state.filtersVModel.static.page = page
    state.filtersVModel.static.sort = sort
  }
}

import Vue from 'vue'
import Vuex from 'vuex'

import category from './modules/category'
import modal from './modules/modal'

Vue.use(Vuex)

export default function createStore (context, API) {
  const store = new Vuex.Store({
    modules: {
      category: category(API),
      modal: modal()
    },
    strict: process.env.NODE_ENV !== 'production'
  })

  // Pass store object to API
  API.attachStore(store)

  if (module.hot) {
    module.hot.accept(
      [
        './modules/category',
        './modules/modal'
      ],
      () => {
        store.hotUpdate({
          modules: {
            category: require('./modules/category').default(API),
            modal: require('./modules/modal').default(API)
          }
        })
      }
    )
  }

  return store
}
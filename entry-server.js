import createApp from './app.js'
import {startRecord, stopRecord} from './server/timing.js'


// Function adds <link rel="prev||next" href="..."> to head tag
// for pages with pagination

function setPrevAndNextLinks ({currentRoute}, {state}, context) {
  const {currentPage, pagesCount} = state.common.seoPagination,
        next = parseInt(currentPage, 10) + 1,
        prev = parseInt(currentPage, 10) - 1,
        {path} = currentRoute

  if (currentPage === 0) {
    return undefined
  }

  if (next <= pagesCount) {
    context.seo.nextLink = `<link rel="next" href="${SERVER_PATH}${path}?page=${next}" />`
  }

  if (prev > 0) {
    if (prev === 1) {
      context.seo.prevLink = `<link rel="prev" href="${SERVER_PATH}${path}" />`
    }
    else {
      context.seo.prevLink = `<link rel="prev" href="${SERVER_PATH}${path}?page=${prev}" />`
    }
  }

  context.seo.canonicalLink = `<link rel="canonical" href="${SERVER_PATH}${path}"/>`
}

export default context => new Promise((resolve, reject) => {
  startRecord(context, 'createApp()')

  const {app, router, store, bus, promiseBus} = createApp(context)

  stopRecord(context, 'createApp()')

  router.push(context.url)

  router.onReady(
    () => {
      const matchedComponents = router.getMatchedComponents()


      // No components at all. That`s weird case,
      // because we must have at least "App.vue" component
      // so matchedComponents shouldn't be empty

      if (!matchedComponents.length) {
        reject({code: 500})
      }
      else {
        startRecord(context, 'REST overall')

        const promiseList = matchedComponents.map(component => {
          const asyncData = component.asyncData

          if (asyncData) {
            return asyncData(store, router, context, bus, promiseBus)
          }

          return Promise.resolve()
        })

        Promise.all(promiseList)
          .then(() => {
            stopRecord(context, 'REST overall')
            context.state = store.state

            // We manually handle 404 redirect, because
            // technically it isn't redirect, it is just router.push()

            if (context.status.code === 404) {
              router.push('/notfound', () => resolve(app))
            }
            else {
              setPrevAndNextLinks(router, store, context)

              resolve(app)
            }
          })
          .catch(reject)
      }
    }
    , reject
  )
})
import bluebird from 'bluebird/js/browser/bluebird.core.min'

if (typeof window === 'undefined') {
  global.Promise = bluebird
}
else {
  require('intersection-observer')
  window.Promise = bluebird;

  (function closestPolyfill (ELEMENT) {
    ELEMENT.matches = ELEMENT.matches ||
                      ELEMENT.mozMatchesSelector ||
                      ELEMENT.msMatchesSelector ||
                      ELEMENT.oMatchesSelector ||
                      ELEMENT.webkitMatchesSelector

    function closest (selector) {
      if (!this) {
        return null
      }
      if (this.matches(selector)) {
        return this
      }
      if (!this.parentElement) {
        return null
      }

      return this.parentElement.closest(selector)
    }

    ELEMENT.closest = ELEMENT.closest || closest
  })(Element.prototype)
}

if (!Array.prototype.find) {
  Array.prototype.find = predicate => {
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined')
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function')
    }

    const list = Object(this),
          listLength = list.length >>> 0,
          thisArg = arguments[1]
    let value

    for (let i = 0; i < listLength; i++) {
      value = list[i]

      if (predicate.call(thisArg, value, i, list)) {
        return value
      }
    }

    return undefined
  }
}

if (!Array.prototype.findIndex) {
  Array.prototype.findIndex = predicate => {
    if (this === null) {
      throw new TypeError('Array.prototype.findIndex called on null or undefined')
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function')
    }

    const list = Object(this),
          listLength = list.length >>> 0,
          thisArg = arguments[1]
    let value

    for (let i = 0; i < listLength; i++) {
      value = list[i]

      if (predicate.call(thisArg, value, i, list)) {
        return i
      }
    }

    return -1
  }
}

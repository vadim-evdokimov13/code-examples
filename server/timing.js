function measureTime (timings) {
  if (timings) {
    const [s, ns] = process.hrtime(timings)

    return (s * 1e9 + ns) / 1e6
  }

  return process.hrtime()
}

function setServerTimingHeader ({recordsMap}, respObject) {
  const formattedRecords = []
  let index = 0

  for (const [recordName, {time, done}] of recordsMap) {
    if (!done) {
      throw Error(`Record ${recordName} was not completed!`)
    }

    const formattedRecord = `ssr${index}; dur=${time}; desc="SSR (${recordName})"`

    formattedRecords.push(formattedRecord)

    index++
  }

  respObject.setHeader('Server-Timing', formattedRecords.join(','))
}

function startRecord ({recordsMap}, key) {
  // Prevent multiple setting for the same keys
  if (recordsMap.has(key)) {
    throw Error(`Record ${key} already exists in timing records map.`)
  }

  const record = {
    time: measureTime(),
    done: false
  }

  recordsMap.set(key, record)
}

function stopRecord ({recordsMap}, key) {
  if (!recordsMap.has(key)) {
    throw Error(`Record ${key} doesn't exists.`)
  }

  const record = recordsMap.get(key)

  // Check if record was already completed
  if (record.done) {
    throw Error(`Record ${key} already done.`)
  }

  record.time = measureTime(record.time)
  record.done = true
}

module.exports = {
  startRecord,
  stopRecord,
  setServerTimingHeader
}
const currencyRegExp = /(\d)(?=(\d{3})+( |$))/g
const noop = () => {}

export default function (Vue) {
  const prototype = Vue.prototype

  // Format number to currency format 1234 -> '1 234'
  prototype.mFmt = num => num.toFixed().replace(currencyRegExp, '$1 ')

  prototype.img = url => {
    if (!url || url.indexOf('missing') !== -1) {
      return ''
    }

    return `background-image: url('${SERVER_PATH}${url}')`
  }
  prototype.imgNoBack = url => `${SERVER_PATH}${url}`

  prototype.modal = function modal (modalData) {
    const that = this,
          bus = that.bus
    let modalDataObj

    // Normalize modal data to obj
    if (modalData instanceof Object) {
      modalDataObj = modalData
    }
    else if (typeof modalData === 'string') {
      modalDataObj = {
        name: modalData,
        data: null
      }
    }
    else {
      throw Error('Wrong modal data. It must be Object{name, data} or String!')
    }

    return new Promise(resolve => {
      if (!this.$isServer) {
        modalDataObj = Object.assign(
          // Add resolve fn
          {close: resolve},
          modalDataObj
        )
      }
      else {
        modalDataObj = Object.assign(
          {close: null},
          modalDataObj
        )
      }

      bus.$emit('setModal', modalDataObj)
    })
  }

  Object.defineProperties(prototype, {
    bus: {
      get () {
        return this.$root.bus
      }
    },
    promiseBus: {
      get () {
        return this.$root.promiseBus
      }
    }
  })
}
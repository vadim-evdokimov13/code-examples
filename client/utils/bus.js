import Vue from 'vue'

function createPromiseBus () {
  return {
    _promiseMap: {},
    _resolveMap: {},
    set (key, promise, override = false) {
      const promiseMap = this._promiseMap,
            resolveMap = this._resolveMap

      if (promiseMap.hasOwnProperty(key)) {
        if (resolveMap.hasOwnProperty(key)) {
          return promise.then(resolveMap[key])
        }

        if (!override) {
          return console.info(`PromiseBus: Multiple promise addition for key ${key}`)
        }
      }

      promiseMap[key] = promise

      return promise
    },
    when (key) {
      const promiseMap = this._promiseMap,
            resolveMap = this._resolveMap

      if (promiseMap.hasOwnProperty(key)) {
        return promiseMap[key]
      }

      const emptyPromise = new Promise(res => {
        resolveMap[key] = res
      })

      promiseMap[key] = emptyPromise

      return emptyPromise
    }
  }
}

export default function createBuses () {
  return {
    bus: new Vue(),
    promiseBus: createPromiseBus()
  }
}
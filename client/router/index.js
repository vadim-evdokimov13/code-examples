import Vue from 'vue'
import VueRouter from 'vue-router'
import qs from 'qs'

import App from '../components/App.vue'
import Category from '../components/pages/Category.vue'
import NotFound from '../components/pages/NotFound.vue'

Vue.use(VueRouter)

export default function createRouter () {
  return new VueRouter({
    mode: 'history',
    linkActiveClass: 'g-active',
    caseSensitive: true,
    base: '/',
    routes: [
      {
        path: '/',
        component: App,
        children: [
          {path: 'u/:userId(\\d+)/:catId(\\d+-[-A-Za-z\\d]+)?', component: Category, props: true},
          {path: 'c/:catId', component: Category, props: true},


          {path: '*', component: NotFound, meta: {notfound: true}}
        ]
      }
    ],
    scrollBehavior: (to, from, savedPosition) => {
      if (to.hash) {
        return {selector: to.hash}
      }
      else if (savedPosition) {
        return savedPosition
      }

      return {x: 0, y: 0}
    },
    parseQuery: str => qs.parse(str, {arrayFormat: 'index'})
  })
}